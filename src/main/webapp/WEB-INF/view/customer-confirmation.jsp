<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Customer Confirmation</title>

</head>
<body>
    The customer is confirmed: ${customer.firstName} ${customer.lastName}
    <br>
    Free passes: ${customer.freePasses}
    <br>
    Postal code: ${customer.postalCode}
    <br>
    Course code: ${customer.courseCode}
</body>
</html>