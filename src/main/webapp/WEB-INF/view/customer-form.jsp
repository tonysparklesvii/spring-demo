<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Customer Registration Form</title>
    <style>
        .error {
            color: red
        }
    </style>
</head>
<body>
<form:form action="processform" modelAttribute="customer">
    First name: <form:input path="firstName"/>
    <br>
    Last name (*): <form:input path="lastName" />
    <form:errors path="lastName" cssClass="error" />
    <br>
    Free passes: <form:input path="freePasses" />
    <form:errors path="freePasses" cssClass="error" />
    <br>
    Postal code: <form:input path="postalCode" />
    <form:errors path="postalCode" cssClass="error" />
    <br>
    Course code: <form:input path="courseCode" />
    <form:errors path="courseCode" cssClass="error" />
    <br>
    <input type="submit" value="Submit"/>
</form:form>
</body>
</html>