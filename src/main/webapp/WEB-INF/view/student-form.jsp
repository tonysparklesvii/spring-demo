<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Student - Input Form</title>
</head>
<body>
    <form:form action="processform" modelAttribute="student">
        First name: <form:input path="firstName" />
        <br>
        Last name: <form:input path="lastName" />
        <br>
        Country:
        <form:select path="country">
            <form:options items="${student.countryOptions}" />
        </form:select>
        <br>
        <form:radiobuttons path="favouriteLanguage" items="${student.favouriteLanguageOptions}" />
        <br>
        Linux <form:checkbox path="operatingSystems" value="Linux" />
        Windows <form:checkbox path="operatingSystems" value="Windows" />
        Mac <form:checkbox path="operatingSystems" value="Mac" />
        <br>
        <input type="submit" value="Submit" />
    </form:form>
</body>
</html>