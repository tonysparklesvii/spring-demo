package com.springtesting.demo.model;

import com.springtesting.demo.validation.CourseCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;

/**
 * Created by tony on 18.05.19
 */

@Getter
@Setter
@NoArgsConstructor
public class Customer {

    private String firstName;
    //Validation
    @NotNull(message = "is required")
    @Size(min=1, message = "is required")
    private String lastName;

    //Validation
    @Pattern(regexp="[a-zA-Z0-9]{5}", message="only 5 chars or digits")
    private String postalCode;

    //Validation
    @NotNull(message="is required")
    @Min(value = 0, message = "must be greater than or equal to 0")
    @Max(value = 10, message = "must be smaller than or equal to 10")
    private Integer freePasses;

    @CourseCode(value = "TONY", message = "course code must start with TONY")
    private String courseCode;

}
