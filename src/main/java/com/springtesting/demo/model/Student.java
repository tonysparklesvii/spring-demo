package com.springtesting.demo.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.LinkedHashMap;

/**
 * @author tonytruong on 17/05/19
 * @project spring-demo-app
 */

@Getter
@Setter
public class Student {
    private String firstName;
    private String lastName;
    private String country;
    private String favouriteLanguage;
    private String[] operatingSystems;
    private LinkedHashMap<String, String> countryOptions;
    private LinkedHashMap<String, String> favouriteLanguageOptions;

    public Student() {
        countryOptions = new LinkedHashMap<>();
        countryOptions.put("BR", "Brazil");
        countryOptions.put("DE", "Germany");
        countryOptions.put("TR", "Turkey");
        countryOptions.put("IN", "India");
        countryOptions.put("US", "America");
        favouriteLanguageOptions = new LinkedHashMap<>();
        favouriteLanguageOptions.put("Java", "Java");
        favouriteLanguageOptions.put("PHP", "PHP");
        favouriteLanguageOptions.put("Ruby", "Ruby");
        favouriteLanguageOptions.put("Python", "Python");
        favouriteLanguageOptions.put("JavaScript", "JavaScript");
    }
}
