package com.springtesting.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * @author tonytruong on 17/05/19
 * @project spring-demo-app
 */

@Controller
@RequestMapping("/hello")
public class HelloWorldController {

    // need a controller method to show the initial HTML form
    @RequestMapping("/showform")
    public String showForm() {
        return "helloworld-form";
    }

    // need a controller method to processs the HTML form
    @RequestMapping("/processForm")
    public String processForm() {
        return "helloworld";
    }

    // new controller method to read form data and add data to the model
    @RequestMapping("/shout")
    public String shout(HttpServletRequest request, Model model) {
        // read the request param from the HTML form
        String theName = request.getParameter("studentName");
        // convert the data to all caps
        theName = theName.toUpperCase();
        // create the message
        String message = "Yo! " + theName;
        // add message to the model
        model.addAttribute("message", message);

        return "helloworld";
    }

    // new controller method to read form data and add data to the model
    @RequestMapping("/friend")
    public String friend(@RequestParam("studentName") String theName, Model model) {
        // convert to uppercase
        theName = theName.toUpperCase();
        // create the message
        String message = "Yo! ma boii " + theName;
        // add message to the model
        model.addAttribute("message", message);

        return "helloworld";
    }
}
