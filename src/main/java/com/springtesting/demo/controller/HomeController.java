package com.springtesting.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author tonytruong on 17/05/19
 * @project spring-demo-app
 */

@Controller
public class HomeController {

    @RequestMapping("/")
    public String home() {
        return "main-menu";
    }
}
