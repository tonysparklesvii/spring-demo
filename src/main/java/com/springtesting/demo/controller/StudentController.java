package com.springtesting.demo.controller;

import com.springtesting.demo.model.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author tonytruong on 17/05/19
 * @project spring-demo-app
 */

@Controller
@RequestMapping("/student")
public class StudentController {

    @RequestMapping("/showform")
    public String showForm(Model model) {
        // create a student object
        Student student = new Student();
        // add student object to the model
        model.addAttribute("student", student);
        return "student-form";
    }

    @RequestMapping("/processform")
    public String processform(@ModelAttribute("student") Student student) {

        return "student-confirmation";
    }
}
