package com.springtesting.demo.controller;

import com.springtesting.demo.model.Customer;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

/**
 * Created by tony on 18.05.19
 */

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @RequestMapping("/showform")
    public String showform(Model model) {
        model.addAttribute("customer", new Customer());
        return "customer-form";
    }

    @RequestMapping("/processform")
    public String processForm(
            @Valid @ModelAttribute("customer") Customer customer,
            BindingResult bindingResult) {
        System.out.println("\n\n\n___________");
        System.out.println("Last name: |" + customer.getLastName() + "|");
        System.out.println("Binding result: " + bindingResult);
        System.out.println("__________\n\n\n");
        if(bindingResult.hasErrors()) {
            return "customer-form";
        }
        return "customer-confirmation";
    }

    /*
    this method preprocesses incoming web requests, and trims whitespaces from all strings
     */
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }
}
