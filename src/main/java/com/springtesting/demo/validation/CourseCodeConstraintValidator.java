package com.springtesting.demo.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by tony on 19.05.19
 */
public class CourseCodeConstraintValidator implements ConstraintValidator<CourseCode, String> {

   private String[] coursePrefix;

   public void initialize(CourseCode constraint) {
      coursePrefix = constraint.value();
   }

   public boolean isValid(String obj, ConstraintValidatorContext context) {
      boolean result = false;

      if(obj != null) {
         for(String prefix : coursePrefix) {
            result = obj.startsWith(prefix);
            if(result) {
               break;
            }
         }
      } else {
         return true;
      }

      return result;
   }
}
