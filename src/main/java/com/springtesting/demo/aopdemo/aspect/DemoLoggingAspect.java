package com.springtesting.demo.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * Created by tony on 19.06.19
 */

@Aspect
@Component
public class DemoLoggingAspect {

    // this is where we add all of our related advices for logging

    // let's start with @Before advice

    @Before("execution(public void add*())")
    public void beforeAddAccountAdvice() {
        System.out.println("||||||||||| EXECUTING @BEFORE ADVICE||||||||||");
    }
}
