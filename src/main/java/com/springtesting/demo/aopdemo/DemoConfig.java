package com.springtesting.demo.aopdemo;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Created by tony on 19.06.19
 */

@Configuration
@EnableAspectJAutoProxy
@ComponentScan("com.springtesting.demo.aopdemo")
public class DemoConfig {
}
