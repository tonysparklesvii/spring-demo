package com.springtesting.demo.aopdemo.dao;

import org.springframework.stereotype.Component;

/**
 * Created by tony on 20.06.19
 */

@Component
public class MembershipDao {

    public void addAccount() {
        System.out.println(getClass() + ": doing stuff adding a membership account");
    }
}
