package com.springtesting.demo.aopdemo.dao;

import org.springframework.stereotype.Component;

/**
 * Created by tony on 19.06.19
 */

@Component
public class AccountDao {

    public void addSillyMember() {
        System.out.println(getClass() + " doing my job");
    }

}
