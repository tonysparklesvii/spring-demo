package com.springtesting.demo.aopdemo;

import com.springtesting.demo.aopdemo.dao.AccountDao;
import com.springtesting.demo.aopdemo.dao.MembershipDao;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by tony on 19.06.19
 */
public class MainDemoApp {

    public static void main(String[] args) {

        // read spring config class
        AnnotationConfigApplicationContext configApplicationContext =
                new AnnotationConfigApplicationContext(DemoConfig.class);

        // get the bean from spring container
        AccountDao accountDao = configApplicationContext.getBean("accountDao", AccountDao.class);

        // call the business method
        accountDao.addSillyMember();

        MembershipDao membershipDao = configApplicationContext.getBean("membershipDao", MembershipDao.class);

        membershipDao.addAccount();

        // close the context
        configApplicationContext.close();
    }
}
